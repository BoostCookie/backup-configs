.PHONY: install, uninstall, howto

howto:
	$(info Use `sudo make install` or `sudo make uninstall`)

install:
	mkdir -p /usr/local/bin
	cp src/backup-configs.py /usr/local/bin/backup-configs

uninstall:
	rm /usr/local/bin/backup-configs
