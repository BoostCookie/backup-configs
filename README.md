# backup-configs

Creates a mirror of your filesystem with only the specified files.

## Configuration
Create the config file `/etc/backup-configs/backup-configs.conf`. It could look something like this:
```
dest_root=/mybackups/lukespc/root

[/]
my/good/file
his/bad/directory

[/etc]
backup-configs
pacman.conf
pacman.d/mirrorlist
fstab
crypttab
cryptsetup-keys.d
locale.gen
locale.conf
vconsole.conf
hostname
hosts
mkinitcpio.conf
wireguard

[/home/myuser]
.ssh
.config
.local/bin
.zshenv
```

`dest_root` describes where to backup your configs. An `my/entry` under `[/group/path]` gets assemblied as `/group/path/my/entry` and gets backed up to `<dest_root>/group/path/my/entry` using `rsync`.

## Create Backups
```sh
sudo backup-configs [--dry-run]
```

At the path described after `dest_root=` a new root directory structure gets created. If there is an entry like `/my/very/important/file` in the config then the directories `<dest_root>/my/very/important` get created and `rsync` is used to copy `file` over. Permissions, owner and group are carried over.

Everything in `<dest_root>` that is not described by the config file gets deleted. For example if there is a file/directory `<dest_root>/my/not_in_config` then it gets deleted, because it is not in the config and neither is any subdirectory or file of it.

Then all the contents of the parent directory of `<dest_root>` (here `/mybackups/lukespc/`) are added to a tar archive `<dest_root>/../<parent_dir_name>.configs.tar.zst`.

When using `--dry-run` the program just shows you the commands it would run without actually doing them.

## Restore All Backups
```sh
sudo rsync -a path/to/dest_root/ /
```
The trailing slash after `dest_root` is important.
Make sure to do this after installing all affected programs, or otherwise your package manager might complain that the config files already exist.

## Installation

### Arch
Simply use the `PKGBUILD`:
```sh
# in an empty directory, get the PKGBUILD
curl -O "https://gitlab.com/BoostCookie/backup-configs/-/raw/master/PKGBUILD"
# install
makepkg -si
# uninstall
sudo pacman -Rsn backup-configs
```

### Other Systems
Make sure to have the dependency `rsync` installed.
```sh
# install
sudo make install
# uninstall
sudo make uninstall
```
